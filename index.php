<!DOCTYPE html>
<html lang="fr">
<head>

  <?php include 'google.php';?>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body>
  <header class="header" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="#header">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>
      <div class="backgroundVideo" data-vide-bg="dist/img/video/headerVideo" data-vide-options="loop: true, muted: true, position: 50% 50%"></div>
      <div class="headerContent text-centered">
          <div class="container">
              <div class="twelve colums">
                  <h1>Faites décoller vos récompenses</h1>
                  <p>Les points Avios sont la devise de fidélité des compagnies de renom telles que British Airways, Iberia, Aer Lingus, Level et Vueling.<br class="hideMobile">Ils permettent d'élargir votre fichier clients et de fidéliser ceux déjà existants</p>
                  <div class="internalLink">
                      <a href="#fourthSectionLink" class="buttonAvios">Téléchargez le livre blanc</a>
                  </div>
                  
              </div>
          </div>
          <div class="downArrow">
              <svg class="arrows">
                    <path class="a1" d="M0 0 L30 32 L60 0"></path>
                </svg>
          </div>
      </div>
      <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span> 
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> - 
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
      
  </header>
  <section class="contentArea whiteContentArea padding" id="firstSectionLink">
      <div class="container">
          <div class="twelve columns">
              <h3 class="text-centered">Avios est la devise de fidélité pour toutes ces companies aériennes</h3>
              <div class="row internalLogos">
                  <div class="nine columns">
                      <div class="row">
                          <div class="four columns logoColumn">
                              <img src="dist/img/A2-aer-lingus-logo.svg" class="aerLingus" alt="Aer Lingus Logo" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/A2-british-airways-logo.svg" class="BAairlines" alt="British Airways Logo" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/BWT4990_Open%20Skies_LOGO_CMYK_POS.svg" class="openSkies" alt="British Airways Open skies" />
                          </div>
                      </div>
                      <div class="row">
                          <div class="four columns logoColumn">
                              <img src="dist/img/Flybe_Logo.svg" class="flyBelogo" alt="Flybe Logo" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/A2-iberia-logo.svg" class="iberiaLogo" alt="Iberia Logo" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/Kulula_logo.svg" class="kululalogo" alt="Kulula Logo" />
                          </div>
                      </div>
                      <div class="row">
                          <div class="four columns logoColumn">
                              <img src="dist/img/level.svg" class="levelLogo" alt="Level Logo" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/meridiana.svg" class="meridianaLogo" alt="" />
                          </div>
                          <div class="four columns logoColumn">
                              <img src="dist/img/A2-vueling-logo.svg" class="vuelingLogo" alt="" />
                          </div>
                      </div>
                  </div>
                  <div class="three columns">
                      <div class="sideLogo">
                          <img src="dist/img/AviosLogo.svg" class="scale-with-grid" />
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="contentArea blueContentArea blueContentAreaBGImageOne padding">
      <div class="container noPaddingMobile">
          <div class="twelve columns">
              <div class="three columns mobileBox">
                  <div class="icon iconNumber">
                      <img src="dist/img/725New.svg" class="scale-with-grid" alt="725 icon" />
                  </div>
                  <p class="remove-bottom"><strong>Passez à la vitesse supérieure</strong></p>
                  <p>Mettez à profit notre fichier croissant de 725 000 membres et voyageurs fréquents en France et de 8 millions de clients actifs dans le monde, partageant le goût des voyages.</p>
              </div>
              <div class="three columns mobileBox">
                  <div class="icon iconPlane">
                      <img src="dist/img/A1-spend-icon-flights.svg" alt="plane icon" />
                  </div>
                  <p class="remove-bottom"><strong>Consolidez la fidélité de votre clientèle</strong></p>
                  <p>Rendez votre programme de fidélité plus attractif en élargissant votre éventail de récompenses client.</p>
              </div>
              <div class="three columns mobileBox">
                  <div class="icon iconEarth">
                      <img src="dist/img/globe.svg" alt="globe image" />
                  </div>
                  <p class="remove-bottom"><strong>Donnez des ailes à votre stratégie marketing</strong></p>
                  <p>Accédez aux communications multicanales des compagnies aériennes et aux données statistiques clients. En attirant de nouveaux clients et en stimulant ceux déjà existants, vous augmenterez leur valeur commerciale pour votre activité à travers le monde.</p>
              </div>
              <div class="three columns mobileBox">
                  <div class="icon iconStar">
                      <img src="dist/img/star.svg" alt="star icon" />
                  </div>
                  <p class="remove-bottom"><strong>Distinguez-vous de vos concurrents</strong></p>
                  <p>Vous démarquer de vos concurrents permet de valoriser votre entreprise, en couvrant une clientèle plus large tout en diversifiant les récompenses proposées.</p>
              </div>
          </div>
      </div>
  </section>
  <section class="contentArea greyContentArea padding">
      <div class="container noPaddingMobile">
          <div class="twelve columns">
              <div class="six columns mobileBox">
                  <p class="remove-bottom"><strong>Améliorez votre performance</strong></p>
                  <p>Forts de 30 ans d’expérience dans les primes voyages, nous mettons notre professionnalisme à votre service : nous vous proposons plusieurs options de collaboration, de l’adhésion de base à une relation privilégiée.</p>
                  <p><strong>Découvrez la vidéo sur les avantages de notre devise universelle pour votre activité</strong></p>
              </div>
              <div class="six columns">
                  <div class="videoWrapper">
                      <iframe width="100%" height="auto" src="https://www.youtube.com/embed/Z_U4h3ayKMA?rel=0&amp;fs=0&amp;showinfo=0" frameborder="0"  allowfullscreen></iframe>
                  </div>
              </div>
          </div>
      </div>
  </section> 
  <section class="contentArea whiteContentArea padding" id="secondSectionLink">
      <div class="container">
          <div class="twelve columns">
              <h3 class="text-centered">Donnez des ailes à votre stratégie marketing</h3>
              <div class="eight columns offset-by-two">
                  <p class="text-centered">Avec un fichier croissant de 725 000 membres en France et de 8 millions de clients actifs dans le monde, donnez de l’ampleur à votre gestion de la relation client.</p>
                  <a href="#" class="buttonAvios buttonHidden openNextSection">Apprendre encore plus <i class="arrow down"></i></a>
              </div>
          </div>
      </div>
  </section>
  <section class="contentArea blueContentArea blueContentAreaBGImageTwo padding hiddenMobile hiddenMobileOne">
      <div class="container noPaddingMobile">
          <div class="twelve columns">
              <div class="three columns mobileBox">
                  <div class="icon iconPerson">
                      <img src="dist/img/personIcon.svg" alt="" />
                  </div>
                  <p class="remove-bottom"><strong>Touchez un large public</strong></p>
                  <p>Grâce au réseau de compagnies aériennes et de leurs partenaires commerciaux, vous aurez accès à un large éventail de voyageurs fréquents, allant par exemple du membre British Airways au profil senior, à une clientèle plus jeune chez Vueling.</p>
              </div>
              <div class="three columns mobileBox">
                  <div class="icon targetIcon">
                      <img src="dist/img/pointerIcon.svg" alt="" />
                  </div>
                  <p class="remove-bottom"><strong>Améliorez votre expertise</strong></p>
                  <p>Vous aurez également accès à de communications multicanales et données statistiques, afin de mieux cibler et rentabiliser vos campagnes marketing à destination de vos nouveaux clients.</p>
              </div>
          </div>
      </div>
  </section>
  <section class="parallaxSection parallaxSectionOne paddingLarge paraxify hiddenMobile hiddenMobileTwo">
      <div class="container">
          <div class="twelve columns">
              <div class="six columns offset-by-six">
                  <div class="whiteBackground">
                      <h4>Pourquoi ne pas  devenir partenaire et essayer les points Avios pour mesurer les retombées clients sur votre activité?</h4>
                      <a href="mailto:karin.drylie@avios.com" class="buttonAvios">Faites une demande d’essai </a>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="mobileDivider"></section>
  <section class="contentArea whiteContentArea padding" id="thirdSectionLink">
      <div class="container">
          <div class="twelve columns">
              <h3 class="text-centered">Élargissez vos récompenses</h3>
              <div class="eight columns offset-by-two">
                <p class="text-centered">En enrichissant votre programme de nouvelles récompenses ludiques à explorer, vous susciterez le goût du voyage chez vos clients, notamment auprès d’une clientèle privilégiée qui aime se faire plaisir.</p>
                  <a href="#" class="buttonAvios buttonHidden openNextSectionTwo">Apprendre encore plus <i class="arrow down"></i></a>
              </div>
          </div>
      </div>
  </section>
  <section class="contentArea blueContentArea blueContentAreaBGImageThree padding hiddenMobile">
      <div class="container noPaddingMobile">
          <div class="twelve columns">
              <div class="row">
                  <div class="three columns mobileBox">
                      <div class="icon iconStar">
                          <img src="dist/img/star.svg" alt="" />
                      </div>
                      <p class="remove-bottom"><strong>Devenez plus attractif</strong></p>
                      <p>En élargissant votre éventail de récompenses, vous pourrez également attirer de nouveaux clients.</p>
                      <p>Une sélection de récompenses avantageuses consolide la fidélité et les dépenses de votre clientèle.</p>
                  </div>
                  <div class="three columns mobileBox">
                      <div class="icon euroIcon">
                          <img src="dist/img/euroIcon.svg" alt="" />
                      </div>
                      <p class="remove-bottom"><strong>Boostez votre ROI</strong></p>
                      <p>En offrant une multitude d'avantages à vos clients, vous garantissez un retour sur investissement. L’étendue de votre catalogue de récompenses vous permettra également de vous démarquer.</p>
                  </div>
                  <div class="three columns mobileBox">
                      <div class="icon spendIcon">
                        <img src="dist/img/Icon_Spend.svg" class="scale-with-grid"  alt="" />
                      </div>
                      <p class="remove-bottom"><strong>Répondez aux attentes de<br>vos clients</strong></p>
                      <p>Un programme avantageux par rapport au programme de fidélité d’Air France : </p>
                      <p class="remove-bottom"><strong>Faites voyager vos clients</strong></p>
                      <p>Un vol aller-retour pour Londres coûte <span style="color: #FFD518;font-family: 'Titillium Web', sans-serif;">8 000 points Avios vs 17 000 Miles Flying Blue</span> (113% de différence / une réduction de 113%) *</p>
                  </div>
                  <div class="three columns mobileBox">
                      <div class="icon iconEarth">
                          <img src="dist/img/globe.svg" alt="" />
                      </div>
                      <p class="remove-bottom"><strong>Faites décoller votre entreprise</strong></p>
                      <p>Nous vous donnons également accès à de nouvelles communications multicanales et données statistiques clients, afin de mieux comprendre et analyser leurs habitudes d’achat pour améliorer votre rentabilité.</p>
                  </div>
              </div>
              <div class="row addTop">
                  <div class="three columns offset-by-six">
                      <p class="smallPara">*Comparaison des vols sur avios.com et flyingblue.com – Octobre 2017</p>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <section class="parallaxSection parallaxSectionTwo paddingLarge paraxify hiddenMobile hiddenMobileThree" >
      <div class="container">
          <div class="twelve columns">
              <div class="six columns offset-by-six">
                  <div class="whiteBackground">
                      <h4>Pourquoi ne pas  devenir partenaire et essayer les points Avios pour mesurer les retombées clients sur votre activité?</h4>
                      <a href="mailto:karin.drylie@avios.com" class="buttonAvios">Faites une demande d’essai </a>
                  </div>
              </div>
          </div>
      </div>
  </section>
 <section class="mobileDivider"></section>
  <section class="contentArea greyContentArea padding" id="fourthSectionLink">
      <div class="container">
          <div class="twelve columns">
              <h3 class="text-centered">Découvrez comment optimiser votre clientèle</h3>
              <div class="internalLink">
                  <a href="thanks.php" class="buttonAvios">Téléchargez le livre blanc</a>
              </div>
          </div>
      </div>
  </section>
  <section class="contentArea whiteContentArea padding">
      <div class="container">
          <div class="twelve columns">
              <h3 class="text-centered">Pourquoi la fidélité affecte-t-elle le comportement du client ?</h3>
              <div class="ten columns offset-by-one">
                  <p class="text-centered">Vous souhaitez savoir pourquoi les programmes de fidélité voyages influent sur les décisions d’achat des clients ?</p>
                  <p class="text-centered">Découvrez les théories psychologiques comportementales sur les clients et pourquoi certains sont plus attirés par des récompenses voyages que des remises monétaires.</p>
                  <p class="text-centered">Téléchargez le livre blanc pour savoir comment mettre en œuvre des théories testées et éprouvées issues de la psychologie et des sciences sociales, afin d’élaborer des programmes de fidélité mieux ciblés.</p>
              </div>
          </div>
      </div>
  </section>
  
    <?php include 'footer.php';?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>
  

</body>
</html>
