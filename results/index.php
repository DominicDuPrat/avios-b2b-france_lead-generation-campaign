<?
include("../dist/php/user.php");
$downloads = get_downloads();
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="../dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="../dist/img/favicon.ico" />

</head>

<body>
    <section class="resultsContent">
        <div class="header">
            <div class="container">
                <div class="logo">
                    <a href="#">
                        <img src="../dist/img/AviosLogo.svg" class="logo" alt="avios logo">
                    </a>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="ten columns offset-by-one">
                <h1>Whitepaper Downloads</h1>
                <div class="downloadListing">
                    <table border="0">
                        <tbody>
                            <tr style="border-bottom:1px solid #001F50;">
                                <th width="100">First Name</th>
                                <th width="100">Surname</th>
                                <th>Job Title</th>
                                <th>email</th>
                                <th width="100">Number of<br>downloads</th>
                                <th width="100">Latest download</th>
                            </tr>

                        <?php
                        if (isset($downloads)){
                            if ($downloads){
                            foreach ($downloads as $result){
                                echo'<tr>';											
                                echo('<td>'.$result[0].'</td>' );
                                echo('<td>'.$result[1].'</td>' );
                                echo('<td>'.$result[2].'</td>' );
                                echo('<td>'.$result[3].'</td>' );
                                echo('<td>'.$result[4].'</td>' );
                                $date = explode(" ", $result[5]);
                                echo('<td>'.$date[0].'</td>' );
                                echo'</tr>';
                            }
                            }
                        }	
                        ?>
                        </tbody>	
                    </table>
                </div>
            </div>
        </div>

    </section>	
	<footer class="footer">
      <div class="footerTop">
          <div class="container">
              <div class="twelve columns">
                  <div class="one column footerLogo">
                      <img src="../dist/img/AviosLogo.svg" class="scale-with-grid" alt="avios logo" />
                  </div>
                  <div class="eight columns">
                      <ul class="remove-bottom">
                          <li>
                              <a href="https://www.avios.com/fr/fr/privacy">Confidentialité</a>
                          </li>
                          <li>
                              <a href="https://www.avios.com/fr/fr/security">Sécurité</a>
                          </li>
                          <li>
                              <a href="https://www.avios.com/fr/fr/accessibility">Accessibilité</a>
                          </li>
                          <li>
                              <a href="https://www.avios.com/fr/fr/terms-and-conditions">Conditions Générales</a>
                          </li>
                          <li>
                              <a href="https://www.avios.com/fr/fr/cookie-policy">Politique de cookies</a>
                          </li>
                      </ul>
                  </div>
                  <div class="three columns footerCopyright">
                      <p>&copy; 2017 Avios, Tous droits réservés.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="footerBottom">
          <div class="container">
              <div class="row">
                  <div class="footerLogo">
                      <img src="../dist/img/A2-IAG-logo.svg" class="scale-with-grid IAGLogo" alt="IAG Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/A2-aer-lingus-logo.svg" class="scale-with-grid AerLingusLogo" alt="Aer Lingus Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/A2-british-airways-logo.svg" class="scale-with-grid BALogo" alt="BA Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/BWT4990_Open%20Skies_LOGO_CMYK_POS.svg" class="scale-with-grid BAopenskiesLogo" alt="BA open skies Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/Flybe_Logo.svg" class="scale-with-grid FlyBeLogo" alt="FlyBe Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/A2-iberia-logo.svg" class="scale-with-grid IberiaLogo" alt="Iberia Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/Kulula_logo.svg" class="scale-with-grid KululaLogo" alt="Kulula Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/level.svg" class="scale-with-grid LevelLogo" alt="Level Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/meridiana.svg" class="scale-with-grid meridianaLogo" class="meridianaLogo" alt="" />
                  </div>
                  <div class="footerLogo">
                      <img src="../dist/img/A2-vueling-logo.svg" class="scale-with-grid vuelingLogo" class="vuelingLogo" alt="" />
                  </div>
              </div>
          </div>
      </div>
  </footer>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="../dist/js/scripts.min.js"></script>
    
</body>
</html>
