<!DOCTYPE html>
<html lang="fr">
<head>

    <?php include 'google.php';?>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body class="altStyling">
  <header class="header removePadding" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="/#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="/#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="/">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>
     
  <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span> 
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> - 
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
  </header>
  <section class="internalHeroImage">
      <img src="dist/img/privacy.jpg" class="scale-with-grid" alt="header image" />
      <span class="overlay">
            <svg version="1.1" id="circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
            <path fill="#FFF" d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
            </svg>
      </span>
  </section>
  <section class="contentArea whiteContentArea padding legal">
      <div class="container">
          <div class="twelve columns">
              <h2 class="text-centered">Politique de confidentialité</h2>
              <p>Avios s&#8217;engage à protéger votre vie privée et à respecter les lois applicables à la protection de données personnelles.</p>
              <h3>Utilisation de notre site Internet</h3>
              <p>Ce site Internet appartient et est exploité par le Groupe Avios (AGL) Ltd, Avios est la raison sociale du Groupe Avios (AGL) Ltd.</p>
              <p>Pour améliorer notre vue d&#8217;ensemble de vos centres d'intérêt, il est possible que nous associons les données que vous nous donnez avec d'autres données collectées &#8220;via les cookies&#8221; sur votre utilisation du site. Dans certains cas, il est possible que nous partagions vos données avec d'autres entreprises partenaires pour qu'elles puissent vous mettre à jour sur des promotions et des services qu'elles estiment être intéressants pour vous.</p>
              <h3>Quelles données personnelles sont recueillies et pourquoi?</h3>
              <p>Afin de répondre à vos demandes d'information, il peut vous être demandé de fournir vos coordonnées, telles que votre nom, adresse e-mail, nom de société, adresse postale. Ces renseignements sont utilisés pour les finalités suivantes :</p>
              <ul>
                  <li>Pour être contacté pour en savoir davantage sur le programme Avios</li>
                  <li>Pour recevoir des brochures</li>
              </ul>
              <h3>Cookies</h3>
              <p>Veuillez consulter notre Politique de Cookies pour une description détaillée de notre utilisation de Cookies et comment vous pouvez les désactiver.</p>
              <h3>Autres sites Internet</h3>
              <p>Le site internet contient des liens vers d'autres sites qui ne sont pas couverts par la politique de confidentialité d'Avios. Si vous cliquez sur ces liens et naviguez sur un des ces sites, les propriétaires de ces sites pourront récupérer vos données personnelles, qu'ils utiliseront selon leur propre politique de confidentialité. Veuillez vous assurer d'être familiarisé avec leur politique de confidentialité.</p>
              <p>Vous n&#8217;avez pas besoin d'autoriser votre navigateur à accepter les cookies pour consulter notre site ou pour accéder à nos services. La plupart des navigateurs vous permettent de désactiver la fonction cookies. Si vous souhaitez savoir comment faire, consultez la page Aide sur le menu de votre navigateur.</p>
              <h3>Modifications de la Politique de Confidentialité</h3>
              <p>Il se peut qu&#8217;Avios change ou mette à jour sa Politique de Confidentialité périodiquement. Si la Politique de Confidentialité est modifiée de façon substantielle, Avios affichera un avis sur le site internet pour informer les membres de ce changement, ainsi que la mise à jour de la Politique de Confidentialité sur une période de 30 jours. Avios vous recommande de consulter régulièrement le site et la Politique de Confidentialité pour prendre connaissance de tout changement de cette Politique.</p>
          </div>
      </div>
  </section>
  <?php include 'footer.php';?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>
 
</body>
</html>
