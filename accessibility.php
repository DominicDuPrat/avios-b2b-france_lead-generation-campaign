<!DOCTYPE html>
<html lang="fr">
<head>

  <?php include 'google.php';?>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body class="altStyling">
  <header class="header removePadding" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="/#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="/#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="/">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>
     
  <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span> 
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> - 
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
  </header>
  <section class="internalHeroImage">
      <img src="dist/img/accessibility.jpg" class="scale-with-grid" alt="header image" />
      <span class="overlay">
            <svg version="1.1" id="circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
            <path fill="#FFF" d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
            </svg>
      </span>
  </section>
  <section class="contentArea whiteContentArea padding legal">
      <div class="container">
          <div class="twelve columns">
              <h2 class="text-centered">Accessibilité</h2>
              <h3>Notre site</h3>  
              <p>Chez Avios, nous nous engageons à offrir l&#8217;accessibilité à notre site web.</p>
              <p>Notre objectif est de garantir que notre site internet fonctionne de manière optimale, quels que soient la technologie, le navigateur ou l'appareil que vous utilisez – y compris de rendre notre site accessible aux personnes aveugles ou malvoyantes.</p>
              <p>Nous surveillons en permanence quels navigateurs, appareils, systèmes d&#8217;exploitation et lecteurs d&#8217;écran sont utilisés (anonymement) et comment ils évoluent, afin d&#8217;optimiser l&#8217;utilisation de notre site.</p>
              <h3>Navigateurs et appareils</h3>
              <p>Notre site est compatible avec tous les navigateurs internet. Nous optimisons particulièrement l&#8217;utilisation du site pour les navigateurs les plus courants (liste ci-dessous) et avec des résolutions d&#8217;écran de 1024 x 768 et plus.</p>
              <ul>
                <li>Chrome</li>
                <li>Mobile Safari</li>
                <li>Microsoft Internet Explorer 11 (et plus)</li>
                <li>Safari</li>
                <li>Firefox</li>
              </ul>
              <p>Certains navigateurs plus anciens ne sont pas complètement compatibles d&#8217;un point de vue visuel. Si vous rencontrez des difficultés réduisant l&#8217;utilisation de notre site, nous restons à votre écoute afin d&#8217;examiner les mises à jour indispensables.</p>
              <p>Nous sommes également conscients de la diversité des appareils utilisés pour accéder à ce site. Nous procédons à des tests afin d'optimiser votre expérience sur notre site, quelles que soient les tailles d&#8217;écran sur les appareils iOS et Android ci-dessous.</p>
              <h3>Systèmes Android</h3>
              <ul>
                  <li>540 x 960 (Samsung Galaxy S4 Mini)</li>
                  <li>1080 x 1920 (Samsung S5 or S6)</li>
              </ul>
              <h3>Systèmes iOS</h3>
              <ul>
                  <li>640 x 1136 (iPhone 5S)</li>
                  <li>1080 x 1920 (iPhone 6 Plus)</li>
                  <li>1535 x 2048 (iPad 2 HD)</li>
              </ul>
              <h3>Taille du texte</h3>
              <p>Vous pouvez changer la taille du texte comme indiqué ci-dessous. Vous pouvez revenir à la résolution d&#8217;écran initiale à tout moment en appuyant sur la touche Control&#43;0 (Command&#43;0 pour les utilisateurs Apple)</p>
              <h3>Comment écouter avios.com</h3>
              <p>Certains systèmes d'exploitation possèdent également des fonctions d&#8217;accessibilité intégrées qui permettent de lire un texte à voix haute ou d&#8217;agrandir une partie de l&#8217;écran. Si vous possédez votre propre logiciel de lecture d&#8217;écran, vous constaterez qu&#8217;il fonctionne aisément sur ce site. Si votre logiciel dispose d&#8217;un "Mode Formulaires", nous vous conseillons de ne pas l&#8217;activer.</p>
              <h3>Vitesse du site web</h3>
              <p>Nous savons à quelle point la vitesse d&#8217;un site est important pour vous et nous nous efforçons de rendre le notre le plus rapide possible, mais certains facteurs affectant le temps de réponse échappent à notre contrôle. Ceux-ci incluent votre vitesse de connexion, l&#8217;efficacité de votre fournisseur d&#8217;accès Internet et l&#8217;encombrement du réseau Internet au moment de son utilisation.</p>
              <h3>Plug-ins et aides</h3>
              <p>Les plug-ins et aides ne sont pas essentiels pour les fonctions principales de notre site, mais celui-ci suppose que vous avez :</p>
              <p>Support Flash – généralement inclus dans le navigateur.</p>
              <p>Adobe Reader pour les fichiers .pdf</p>
              <p>Lorsqu&#8217;un plug-in ou application d&#8217;aide est requis pour accéder à un contenu, cela est généralement indiqué.   Les plug-ins peuvent être téléchargés en cliquant sur les liens ci-dessous. Si vous utilisez ces liens, Avios ne peut être tenu responsable des problèmes éventuels associés à ces plug-ins.</p>
              <p><a href="https://get.adobe.com/flashplayer/otherversions/" target="_blank">Adobe Flash Player</a></p>
              <p><a href="https://get.adobe.com/reader" target="_blank">Adobe Reader</a></p>
              <p>Notre site fonctionne également mieux si JavaScript est activé. Si JavaScript n&#8217;est pas encore activé (le site vous avertira), vous pouvez le faire ici.</p>
              
          </div>
      </div>
  </section>
  <?php include 'footer.php';?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>
 
</body>
</html>
