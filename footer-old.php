<footer class="footer">
      <div class="footerTop">
          <div class="container">
              <div class="twelve columns">
                  <div class="one column footerLogo">
                      <img src="dist/img/AviosLogo.svg" class="scale-with-grid" alt="avios logo" />
                  </div>
                  <div class="eight columns">
                      <ul class="remove-bottom">
                          <li>
                              <a href="privacyPolicy.php">Confidentialité</a>
                          </li>
                          <li>
                              <a href="accessibility.php">Accessibilité</a>
                          </li>
                          <li>
                              <a href="termsandConditions.php">Conditions Générales</a>
                          </li>
                          <li>
                              <a href="cookiePolicy.php">Politique de cookies</a>
                          </li>
                      </ul>
                  </div>
                  <div class="three columns footerCopyright">
                      <p>&copy; <?php echo date("Y"); ?>  Avios, Tous droits réservés.</p>
                  </div>
              </div>
          </div>
      </div>
      <div class="footerBottom">
          <div class="container">
              <div class="row">
                  <div class="footerLogo">
                      <img src="dist/img/A2-IAG-logo.svg" class="scale-with-grid IAGLogo" alt="IAG Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/A2-aer-lingus-logo.svg" class="scale-with-grid AerLingusLogo" alt="Aer Lingus Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/A2-british-airways-logo.svg" class="scale-with-grid BALogo" alt="BA Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/BWT4990_Open%20Skies_LOGO_CMYK_POS.svg" class="scale-with-grid BAopenskiesLogo" alt="BA open skies Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/Flybe_Logo.svg" class="scale-with-grid FlyBeLogo" alt="FlyBe Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/A2-iberia-logo.svg" class="scale-with-grid IberiaLogo" alt="Iberia Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/Kulula_logo.svg" class="scale-with-grid KululaLogo" alt="Kulula Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/level.svg" class="scale-with-grid LevelLogo" alt="Level Logo" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/meridiana.svg" class="scale-with-grid meridianaLogo" class="meridianaLogo" alt="" />
                  </div>
                  <div class="footerLogo">
                      <img src="dist/img/A2-vueling-logo.svg" class="scale-with-grid vuelingLogo" class="vuelingLogo" alt="" />
                  </div>
              </div>
          </div>
      </div>
  </footer>