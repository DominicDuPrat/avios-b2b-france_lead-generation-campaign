<!DOCTYPE html>
<html lang="fr">
<head>

    <?php include 'google.php';?>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body class="altStyling">
  <header class="header removePadding" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="/#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="/#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="/">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>
     
  <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span> 
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> - 
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>    
  </header>
  <section class="internalHeroImage">
      <img src="dist/img/terms.jpg" class="scale-with-grid" alt="header image" />
      <span class="overlay">
            <svg version="1.1" id="circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
            <path fill="#FFF" d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
            </svg>
      </span>
  </section>
  <section class="contentArea whiteContentArea padding legal">
      <div class="container">
          <div class="twelve columns">
              <h2 class="text-centered">Conditions générales</h2>
              <p>Ce site internet appartient et est exploité par Avios Group (AGL) Ltd, dont le siège social est situé à Astral Towers, Betts Way, London Road, Crawley, West Sussex RH10 9XY, Royaume Uni.</p>
              <p>Dans les présentes Conditions Générales : &#8220;Nous&#8221;, &#8220;notre&#8221;, &#8220;AGL&#8221; font référence au Groupe Avios (AGL) Ltd alors que les références à &#8220;Vous&#8221; et &#8220;Votre&#8221; désignent les personnes accédant à ce site Internet (y compris les personnes y accédant pour le compte d&#8217;autres personnes) ; les références à &#8220;matériel&#8221; ou &#8220;matériels&#8221; incluent les données, informations et bases de données.</p>
              <p>Veuillez lire attentivement les Conditions Générales avant d&#8217;utiliser ce site internet. En accédant à ce site, vous vous engagez à respecter ces Conditions Générales.</p>
              <h3>Accès et utilisateurs</h3>
              <p>Vous ne pouvez utiliser ce site web que conformément à ces Conditions Générales et, en tout état de cause, uniquement à des fins légitimes et appropriées. Nous pouvons à tout moment modifier ces Conditions Générales, et toute modification de ce type entrera immédiatement en vigueur dès son affichage sur ce site internet. Par conséquent, votre utilisation ou navigation continue de ce site internet est considérée comme valant acceptation et respect des Conditions Générales modifiées (notamment pour les modalités et conditions énoncées dans la Politique de Confidentialité), sans préjudice sur ces conditions spécifiques que celles obtenues dans le cas où vous souhaiterez sous-traiter un des services offerts.</p>
              <p>La communication des informations sur ce site est gratuite (en dehors des coûts applicables par votre fournisseur d&#8217;accès internet). Nonobstant ce qui précède, IAG se réserve le droit d&#8217;exiger votre enregistrement, en complétant un formulaire d&#8217;inscription d&#8217;utilisateur, avant d&#8217;accéder aux services fournis par le site internet.</p>
              <p>L&#8217;accès à ce site est uniquement et exclusivement de votre responsabilité. L&#8217;accès à ce site n&#8217;implique aucun engagement commercial entre vous et AGL.</p>
              <h3>Limitation de responsabilité</h3>
              <p>Les informations publiées sur ce site internet ont été exclusivement rassemblées dans le but de fournir des informations sur AGL et ses partenaires. Les informations de ce site sont fournies &#8220;telles quelles&#8221; et ne sont pas détaillées. Malgré nos efforts, ces renseignements peuvent ne pas être précis, actualisés ou applicables à certains cas, et nous ne fournissons aucune garantie ou représentation de quelque sorte que soit, expresse ou implicite, en rapport avec eux.</p>
              <p>Les informations sur ce site comprennent des déclarations relatives à nos convictions et attentes sur des événements futurs et autres perspectives d&#8217;avenir. Les faits réels peuvent différer considérablement. Les exceptions et restrictions ci-dessus s&#8217;appliquent uniquement dans la limite de ce qui est autorisé par la loi.</p>
              <h3>Liens vers d&#8217;autres sites internet</h3>
              <p>Ce site web peut inclure des liens vers d&#8217;autres sites internet. Nous ne soutenons pas ces sites internet et ne sommes en conséquence nullement responsables des informations, documents, produits ou services contenus ou accessibles par le biais de ces sites. L&#8217;accès et l&#8217;utilisation de ces sites internet se font à votre propre risque.</p>
              <p>Vous pouvez créer un lien vers ce site internet uniquement avec notre autorisation écrite explicite. Nous nous réservons expressément le droit de retirer à tout moment notre autorisation d&#8217;un lien que nous jugeons inapproprié ou sujet à controverse.</p>
              <h3>Droits de propriété</h3>
              <p>Les droits de propriété intellectuelle sur le contenu du site internet, son apparence et ergonomie, le logiciel sous-jacent (incluant les codes sources) et les différents éléments composant le site (textes, images, photos, vidéos, enregistrements audio etc…), ci-après le &#8220;Contenu&#8221;, sont détenus par AGL ou ses concédants.</p>
              <p>Votre utilisation du site n&#8217;implique nullement la cession des droits intellectuels et/ou industriels sur ce site, le Contenu et/ou les signes distinctifs d&#8217;AGL. Par conséquent, sauf dans les cas autorisés par la loi ou avec l&#8217;accord préalable et écrit d&#8217;AGL, il vous est formellement interdit de copier, modifier, distribuer, divulguer publiquement, fournir, vendre, transférer, extraire et/ou réutiliser le site, son Contenu et/ou les signes distinctifs d&#8217;AGL.</p>
              <h3>Divers</h3>
              <p>Les présentes Conditions Générales contiennent toutes les conditions de votre accord avec Nous concernant votre utilisation de ce site internet. Aucune autre déclaration écrite ou orale ne sera intégrée. Certaines pages de ce site peuvent être hébergées par des tiers prestataires de services extérieurs. Nous n&#8217;offrons ni la garantie concernant la fiabilité, la stabilité ou l&#8217;absence de virus de tout logiciel téléchargé sur ce site, ni la disponibilité des sites de téléchargement le cas échéant.</p>
              <p>Tous les logiciels téléchargés depuis notre site ou par l&#8217;intermédiaire d&#8217;un lien indiqué par notre site, sont toujours installés et utilisés à vos propres risques. AGL n&#8217;est en aucun cas responsable de tout dommage causé par l&#8217;intrusion, les omissions, interférences, virus, chevaux de Troie, dérangements ou coupures dus à des raisons indépendantes de sa volonté durant l&#8217;exploitation du système électronique; pour les retards ou blocages durant l&#8217;utilisation causés par des déficiences ou surcharges dans le processeur central de données, les lignes téléphoniques, la connexion internet ou autre système électronique; ni les dommages causés par l&#8217;incursion illégale de parties tierces échappant au contrôle d&#8217;AGL. </p>
              <p>AGL décline toute responsabilité pour tout dommage ou perte que vous pourriez subir suite aux erreurs, défauts ou omissions, d&#8217;une information fournie à une partie tierce sur ce site.</p>
              <p>Vous devez mettre à jour les correctifs de sécurité sur les navigateurs correspondants. AGL ne sera tenu pour responsable que pour les dommages causés par l&#8217;utilisation du site lorsque ceux-ci résultent d&#8217;un dol ou d&#8217;une faute lourde et qu&#8217;aucune faute ne peut vous être incombée.</p>
              <p>Dans le cas où une des dispositions, en tout ou partie, des présentes Conditions Générales, s&#8217;avère non valide, illégale ou non applicable par une autorité compétente, celle-ci sera, à cet égard, sera supprimée des présentes Conditions Générales, sans modifier la validité ou l&#8217;applicabilité des autres dispositions, dans la mesure la plus large permise par la loi.</p>
              <p>Votre utilisation de ce site, tout élément téléchargé, problème, réclamation ou litige découlant ou en relation avec ce site et l&#8217;application des présents statuts sont régis et interprétés conformément aux lois du Royaume-Uni. Sans préjudice de ces dispositions, les tribunaux anglais ont la compétence exclusive pour régler tout différend, qu&#8217;il soit contractuel ou non-contractuel, découlant ou en relation avec ce site et l&#8217;application des présents statuts. Vous acceptez que toute procédure peut être engagée contre vous, et non par, devant les tribunaux de votre pays de résidence.</p>
              <p>Les informations figurant sur le site peuvent ne pas être mises à jour. AGL se réserve le droit d&#8217;actualiser, modifier ou supprimer les informations sur ce site internet, ainsi que d&#8217;en limiter ou refuser l&#8217;accès. De même, AGL se réserve le droit de suspendre, ou ne plus présenter la configuration, les caractéristiques techniques, contenus et services des sites internet, à tout moment et sans préavis.</p>
          </div>
      </div>
  </section>
  <?php include 'footer.php' ;?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>
 
</body>
</html>
