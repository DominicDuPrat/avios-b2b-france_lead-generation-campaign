
// @koala-prepend "paraxify.js"
// @koala-prepend "jquery.vide.js"
// @koala-prepend "cookie.js"

var data = {};

$(document).ready(function(){
    // Add class to header when 10 pixels scrolled
    if ($(window).width() >= 768) {
        $(window).scroll(function() {    
            var scroll = $(window).scrollTop();

            if (scroll >= 10) {
                $(".header").addClass("scrolled");
            } else {
                $(".header").removeClass("scrolled");
            }
        });
        
        
    } else {
        $('body').addClass('mobile');
    }
    
    if ($(window).width() > 1024) {
        //myParaxify = paraxify('.paraxify');
        var myParaxify = paraxify('.paraxify', {
            speed: 1,
            boost: 1
        });
        
    } else {
        
    }
    
    // Window resize event
    $(window).on('resize', function(){
          if ($(window).width() >= 768) {
            $(window).scroll(function() {    
                var scroll = $(window).scrollTop();

                if (scroll >= 10) {
                    $(".header").addClass("scrolled");
                } else {
                    $(".header").removeClass("scrolled");
                }
            });

        } else {
            $('body').addClass('mobile');
        }
        if ($(window).width() > 1024) {
        //myParaxify = paraxify('.paraxify');
            var myParaxify = paraxify('.paraxify', {
                speed: 1,
                boost: 1
            });

        } else {

        }
    });
    
    
    
    
   
    
    
    
	$('#signup').submit(function(e){
      resetErrors();
      var url = 'process.php';
	  var doc = 'http://pc.avios-fr/dist/docs/Avios_Thought_Leadership_Whitepaper.pdf';
	  
      $.each($('form input, form select'), function(i, v) {	  
		 switch (v.type)
		{
			 case "checkbox":
			  if(v.checked){
				  data[v.name] = '1';		 
			  } else {
				  data[v.name] = '0';	
                  
			  }
			 break;
			
		   case "submit":
		   break;
		   
		    default:
			data[v.name] = v.value;		 
			break;
		}  		
      }); //end each	  
	  
	   console.log(data);
	  
      $.ajax({
          dataType: "json",
          type: 'POST',
          url: url,
          data: data,
          success: function(resp) {              
			$('.formSuccessHide').addClass('hidden');
			$('.successMessage').removeClass('hidden');		  
			  
          },
          error: function() {
              console.log('there was a problem checking the fields');
          }
      });
      return false;
        
       
        
        
  });
	
  dropdownLanguage(); 
    
  smoothScroll();
    
  navIconAnimation();
 
});


function resetErrors() {
    $('form input, form select').removeClass('inputTxtError');
    $('label.error').remove();
}

function dropdownLanguage() {
    $('.languageBar .selectize-input').click(function(){
         $(this).toggleClass('open');
         $('.languageBar .selectize-dropdown').toggle();  
         
    });
}

function smoothScroll() {
    // Smooth scroll for internal links
    
    $('.internalLink a[href^="#"]').on('click',function (e) {
	    e.preventDefault();

	    var target = this.hash;
	    var $target = $(target);

	    $('html, body').stop().animate({
	        'scrollTop': $target.offset().top
	    }, 900, 'swing', function () {
	        window.location.hash = target;
	    });
	});
    
    
    // Mobile show and hide content 
    $('.contentArea .openNextSection').click(function(e){
        e.preventDefault();
        $(this).closest('section').next('.hiddenMobile').toggleClass('show');
        $('.hiddenMobileTwo').toggleClass('show');
    });
    $('.contentArea .openNextSectionTwo').click(function(e){
        e.preventDefault();
        $(this).closest('section').next('.hiddenMobile').toggleClass('show');
        $('.hiddenMobileThree').toggleClass('show');
    });
}

function navIconAnimation() {
    
    // Add class to nav icon when clicked
    
    $('#nav-icon').click(function(){
		$(this).toggleClass('open');
        $('.hiddenNavContainer').toggleClass('open');
	});
    
    
    $('.mobile .header .hiddenNavContainer ul li a').click(function(){
        $('.hiddenNavContainer').removeClass('open');
        $('#nav-icon').removeClass('open');
    });
    
}


