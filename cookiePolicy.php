<!DOCTYPE html>
<html lang="fr">
<head>

    <?php include 'google.php';?>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body class="altStyling">
  <header class="header removePadding" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="/#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="/#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="/">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>

  <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span>
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </header>
  <section class="internalHeroImage">
      <img src="dist/img/cookies.jpg" class="scale-with-grid" alt="header image" />
      <span class="overlay">
            <svg version="1.1" id="circle" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 500 250" enable-background="new 0 0 500 250" xml:space="preserve" preserveAspectRatio="none">
            <path fill="#FFF" d="M250,246.5c-97.85,0-186.344-40.044-250-104.633V250h500V141.867C436.344,206.456,347.85,246.5,250,246.5z"></path>
            </svg>
      </span>
  </section>
  <section class="contentArea whiteContentArea padding legal">
      <div class="container">
          <div class="twelve columns">
              <h2 class="text-centered">Notre politique de cookies</h2>
              <h3>Que sont les cookies? </h3>
              <p>Un cookie est une petite quantité d&#8216;informations en format texte qu&#8216;un site web enregistre sur votre ordinateur et mis à disposition de ce site à chaque visite.</p>
              <h3>Que ne sont pas les cookies?</h3>
              <p>Ne vous inquiétez pas, les cookies ne sont pas des virus et nous ne pouvons accéder à vos informations personnelles stockées sur votre ordinateur lorsque vous les acceptez.</p>
              <h3>Pourquoi les cookies sont importants?</h3>
              <p>Ces fichiers anonymes nous aident à mémoriser votre profil et à récolter des informations utiles lors de vos précédentes visites, afin que nous puissions vous communiquer des informations pertinentes sur notre site internet. Avios.com utilise les cookies ci-dessous, chacun dans un but différent. Dans chaque cas, les cookies sont utilisés pour vous offrir une expérience optimale et ne présente aucune menace sur la vie privée.</p>
              <table>
                <tbody>
                    <tr class="header">
                        <td>Cookie</td>
                        <td>Nom</td>
                        <td>But</td>
                    </tr>
                    <tr class="grey">
                        <td><p>Avios</p></td>
                        <td><p>Cookie traceur - variable</p></td>
                        <td><p>Ces cookies sont juste utilisés pour enregistrer votre visite sur le site et la façon dont vous l&#8216;utilisez (aucun renseignement) </p></td>
                    </tr>
                </tbody>
              </table>
              <h3>Refuser/Accepter les Cookies</h3>
              <p>Vous pouvez accepter ou refuser les cookies en modifiant les paramètres de votre navigateur internet.</p>
          </div>
      </div>
  </section>
  <?php include 'footer.php';?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>

</body>
</html>
