<!DOCTYPE html>
<html lang="fr">
<head>

  <?php include 'google.php';?>
  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Avios B2B France Lead Generation Campaign</title>
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="refresh" content="1;URL=download.php">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="dist/css/style.css">

  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="dist/img/favicon.ico" />

</head>
<body>
  <header class="header header-thanks" id="header">
      <nav class="nav">
          <div class="mobileLogo">
              <a href="#">
                  <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
              </a>
          </div>
          <div class="container hiddenNavContainer">
              <div class="twelve columns">
                  <ul class="remove-bottom">
                      <li class="remove-bottom internalLink">
                          <a href="/#firstSectionLink">Une devise universelle</a>
                      </li>
                      <li class="remove-bottom borderRight internalLink">
                          <a href="/#secondSectionLink" >Faites décoller votre marketing</a>
                      </li>
                      <li class="remove-bottom noBorder hiddenMobile internalLink">
                          <a href="/">
                              <img src="dist/img/AviosLogo.svg" class="logo" alt="avios logo" />
                          </a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#thirdSectionLink">Faites décoller vos recompenses</a>
                      </li>
                      <li class="remove-bottom internalLink">
                          <a href="/#fourthSectionLink">Papier blanc</a>
                      </li>
                  </ul>
              </div>
          </div>
          <div id="nav-icon">
              <span></span>
              <span></span>
              <span></span>
              <span></span>
          </div>
      </nav>  
      <div class="languageBar">
        <div class="selectize-control language-select single">
            <div class="selectize-input items full has-options has-items">
                <div class="item" data-value="/uk/en">
                    <span>Français -</span> 
                    <span></span>
                    <img src="dist/img/franceFlag.png">
                    <span></span>
                </div>
                <input type="text" autocomplete="off" tabindex="" readonly="readonly" style="width: 4px; opacity: 0; position: absolute; left: -10000px;">
            </div>
            <div class="selectize-dropdown single language-select" style="display: none;">
                <div class="selectize-dropdown-content">
                    <div>
                        <a href="https://www.aviosgroup.com/uk/en/">
                            <span class="name">Anglais</span> -
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/gb-min.png">
                            </span>
                        </a>
                    </div>
                    <div >
                        <a href="https://www.aviosgroup.com/es/es/">
                            <span class="name">Espanol</span> - 
                            <span class="flag">
                                <img src="https://www.aviosgroup.com/common_assets/img/es-min.png">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
      
  </header>
  <section class="contentArea greyContentArea greyContentAreaForm padding-thanks">
      <div class="container">
          <div class="ten columns offset-by-one">
              
              <div class="successMessage text-centered">
                  <h3>Merci d’avoir téléchargé notre livre blanc sur la psychologie de la fidélité client.</h3>
                  <p>La fidélité client est un sujet passionnant et la clé du succès de compagnies comme la vôtre. Pour plus d’informations sur notre devise universelle Avios, contactez votre directeur commercial.</p>
                  <ul class="remove-bottom">
                      <li>Tél. : +33 (0)6 99 67 87 05</li>
                      <li>Email : <a href="mailto:karin.drylie@avios.com">karin.drylie@avios.com</a></li>
                  </ul>
				  <a class="hidden" id="pdf-download" title="hello"></a>
              </div>
          </div>
      </div>
  </section>
   <?php include 'footer.php';?>
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="dist/js/scripts.min.js"></script>
   
</body>
</html>
