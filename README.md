# README #

This README would normally document whatever steps are necessary to get your application up and running.

### If you don't have koala installed download it. 

* [Download here](http://koala-app.com/)

### Once downloaded ###

Once downloaded koala and installed, open this repo in koala and turn off all files for auto compile except from Skeleton.scss and scripts.js

### Set the correct route for output ###

Right click on either the sass file or js file and set output path, this should go to dist/css/style.css or for the js go to dist/js/scripts.min.js.

### You should be good to go ###

Now koala will run in the background and update the css and js everytime you make a change and save it. 

Enjoy!